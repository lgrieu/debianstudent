#!/bin/sh


#INSTALLATION DES LOGICIELS ET DRIVERS

#on rajoute la possibilité d'utiliser les dépots non libres et les paquets expérimentaux
#à utiliser si certains drivers necesitent une version plus récente, par exemple
cp -r sources.list /etc/apt/

#on met à jour la liste des paquets ainsi modifiée
apt-get update

#on installe les drivers non libres, cela permet de résoudre les problèmes dus aux cartes graphiques notamment
apt-get -y install firmware-linux-nonfree

#on installe les logiciels que l'on veut
apt-get -y install qelectrotech
apt-get -y install cups
apt-get -y install synaptic
apt-get -y install veyon-master
apt-get -y install gnome-shell-extension-caffeine

#on cree un racourci pour configurer le proxy du lycée
cp -r lpcm.desktop /usr/share/applications/

#ce racourci va lancer l'executable suivant
cp -r SetProxy.sh /opt/

#on rend executable le script
chmod +x /opt/SetProxy.sh

#on met le systeme à jour
apt-get update
apt-get -y dist-upgrade
apt -y autoremove

#CREATION D'UN UTILITAIRE DE MISE A JOUR

#on cree un racourci 
cp -r maj.desktop /usr/share/applications/

#on lui donne les permissions voulues
chmod 644 /usr/share/applications/maj.desktop

#ce racourci va lancer l'executable suivant
cp -r maj.sh /opt/

#on rend executable le script
chmod 755 /opt/maj.sh

#on accorde à eleve les droit administrateur, seulement pour executer le scripte de mise a jour
echo "eleve ALL=(ALL) NOPASSWD: /opt/maj.sh" >> /etc/sudoers

#on fait en sorte que l'application se lance au démarrage
cp -r maj.desktop /etc/xdg/autostart

#on lui donne les permissions voulues
chmod 644 /etc/xdg/autostart/maj.desktop

#OPTIMISATION DU SYSTEME

#on demande au systeme de n'utiliser le swap qu'en dernier recours
echo "vm.swappiness=1" >> /etc/sysctl.conf
echo "vm.vfs_cache_pressure=50" >> /etc/sysctl.conf

#on modifie le fichier de configuration de grub pour reduire le temps d'affichage au démarrage
sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=1/g' /etc/default/grub

#on monte les fichiers temporaires dans la RAM
echo "tmpfs /tmp tmpfs defaults 0 0" >> /etc/fstab

#MODIFICATION ESTHETIQUE

#on change le fond d'écran par defaut
cp -r 1920x1080.svg /usr/share/wallpapers/DebianTheme/contents/images/


#INSTALLATION DES DOSSIERS ELEVE DANS LA RAM

#on monte le bureau et les dossiers eleve dans la RAM
#ainsi, à chaque instinction, toutes les donnée sont effacées et le bureau est remis a zero
echo "tmpfs /home/eleve tmpfs defaults 0 0" >> /etc/fstab


  
#on donne les dernières instructions
echo "Installation terminée, entrez la commande: 'su -' puis: 'update-grub'"

exit 0
