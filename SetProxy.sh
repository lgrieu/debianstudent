#!/bin/bash

f_main () {
firefox & sleep 3 && pkill firefox
PREF=`find /home/eleve/.mozilla -name "prefs.js"`
echo 'user_pref("network.predictor.cleaned-up", true);' >> ${PREF}
echo 'user_pref("network.proxy.backup.ftp", "");' >> ${PREF}
echo 'user_pref("network.proxy.backup.ftp_port", 0);' >> ${PREF}
echo 'user_pref("network.proxy.backup.socks", "");' >> ${PREF}
echo 'user_pref("network.proxy.backup.socks_port", 0);' >> ${PREF}
echo 'user_pref("network.proxy.backup.ssl", "");' >> ${PREF}
echo 'user_pref("network.proxy.backup.ssl_port", 0);' >> ${PREF}
echo 'user_pref("network.proxy.ftp", "192.168.232.1");' >> ${PREF}
echo 'user_pref("network.proxy.ftp_port", 3128);' >> ${PREF}
echo 'user_pref("network.proxy.http", "192.168.232.1");' >> ${PREF}
echo 'user_pref("network.proxy.http_port", 3128);' >> ${PREF}
echo 'user_pref("network.proxy.share_proxy_settings", true);' >> ${PREF}
echo 'user_pref("network.proxy.socks", "192.168.232.1");' >> ${PREF}
echo 'user_pref("network.proxy.socks_port", 3128);' >> ${PREF}
echo 'user_pref("network.proxy.ssl", "192.168.232.1");' >> ${PREF}
echo 'user_pref("network.proxy.ssl_port", 3128);' >> ${PREF}
echo 'user_pref("network.proxy.type", 1);' >> ${PREF}
firefox
}

 
f_main

exit 0
