#!/bin/bash

Update () {
    apt-get update
    }
Upgrade () {
    apt-get full-upgrade -y
    }
Autoremove () {
    apt-get autoremove -y
    }

Update
if [ $? -eq 0 ]; then
    echo RECUPERATION DES MISES A JOUR EFFECTUEE AVEC SUCCES
    Upgrade
    if [ $? -eq 0 ]; then
        echo MISE A JOUR DES PAQUETS EFFECTUEE AVEC SUCCES
        Autoremove
        if [ $? -eq 0 ]; then
        echo PAQUETS OBSOLETES SUPRIMES AVEC SUCCES
        
        else
        echo ECHEC DE SUPRESSION DES PAQUETS OBSOLETES
        fi
    else
        echo ECHEC DE MISE A JOUR DES PAQUETS
    fi
else
   echo ECHEC DE RECUPERATION DES MISES A JOUR
fi
