<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initiale-scale=1.0, shrink-to-fit=no, user-scalable=no">
		<link rel="stylesheet" type="text/css" href="Style/index.css">
		<title>Serveur A208</title>
	</head>
	<body>
		<div id="wrapper">
			<h1>Serveur de M. GRIEU Salle A208</h1>
			<div class="contact">
			Contact: <a href="mailto:grieu.prof@gmail.com" title="Ecrire un email">grieu.prof@gmail.com</a>
			</div>
			<nav>
				<ul>
					<li>
						<a href="index.html">Retour à l'accueil</a>
					</li>
				</ul>
			</nav>
<?php

$upload_dir = "Uploads";
$target_dir = $upload_dir. "/" . date("Y_m_d")  ;
$raw_name = basename( $_FILES["fileToUpload"]["name"]);
$imageFileType = strtolower(pathinfo($raw_name,PATHINFO_EXTENSION));

$userName = htmlspecialchars($_POST['nom']);
$uploadOk = 1;

$target_file = $target_dir. "/" . date("H_i_s")."_".$userName.".". $imageFileType;


if (!file_exists($target_dir)) {
    mkdir($target_dir, 0777, true);
    
} 

if (file_exists($target_file)) {
    echo  "Erreur, Fichier Existant.";
    $uploadOk = 0;
}

if ($_FILES["fileToUpload"]["size"] > 1000000) {
    echo  "Erreur, Fichier trop lourd.";
    $uploadOk = 0;
}

if($imageFileType != "pdf" && $imageFileType != "doc" && $imageFileType != "docx" && $imageFileType != "qet" && $imageFileType != "odt" && $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
    echo  "Erreur seul les fichiers de type : .pdf,.doc,.docx,.qet,.odt,.jpg,.png,.jpeg sont valides.";
    $uploadOk = 0;
}

if ($uploadOk == 0) {
    echo  "fichier non envoyé";

} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {	
        echo  "Le Fichier ". $raw_name. " est bien parti.";
    } else {
        echo  "Erreur de chargement.";
    }
}

?>
	</div>
	</body>
</html>